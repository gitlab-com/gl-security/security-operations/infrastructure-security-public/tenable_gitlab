from typing import Any, Dict
from unittest.mock import MagicMock

from gitlab.client import Gitlab
from gitlab.v4.objects.issues import ProjectIssue, ProjectIssueLink
from gitlab.v4.objects.projects import ProjectManager

from tenable_gitlab.config import Config
from tenable_gitlab.utils.gitlab import GitlabUtils


def gitlab_utils(gitlab_client: Gitlab) -> GitlabUtils:
    config = Config()
    return GitlabUtils(
        gitlab_client,
        0,
        config.incident_template_path,
        config.plugin_issue_template_path,
        config.default_incident_labels,
        config.default_plugin_labels,
    )


class TestCreate:
    def test_create_plugin_issue(
        self,
        dummy_gitlab_client: Gitlab,
        random_plugin: Dict[str, Any],
        mocked_gitlab_utils_get_plugin_issue: MagicMock,
        mocked_gitlab_project_issue_manager_create: MagicMock,
    ) -> None:
        dummy_gitlab_utils = gitlab_utils(dummy_gitlab_client)
        dummy_gitlab_utils.create_or_update_plugin_issue(random_plugin)

        mocked_gitlab_utils_get_plugin_issue.assert_called_once_with(
            random_plugin["id"]
        )
        mocked_gitlab_project_issue_manager_create.assert_called_once()

    def test_create_incident(
        self,
        dummy_gitlab_client: Gitlab,
        random_plugin: Dict[str, Any],
        random_asset: Dict[str, Any],
        random_vulnerability: Dict[str, Any],
        mocked_gitlab_utils_get_plugin_issue: MagicMock,
        mocked_gitlab_project_issue_manager_create: MagicMock,
        mocked_gitlab_project_link_issue_manager_create: MagicMock,
    ) -> None:
        dummy_gitlab_utils = gitlab_utils(dummy_gitlab_client)
        dummy_gitlab_utils.create_or_update_incident(
            random_plugin, random_asset, random_vulnerability
        )

        mocked_gitlab_utils_get_plugin_issue.assert_called_with(
            random_plugin["id"]
        )
        assert mocked_gitlab_project_issue_manager_create.call_count == 2
        mocked_gitlab_project_link_issue_manager_create.assert_called_once_with(
            {"target_project_id": 0, "target_issue_iid": 0}
        )

    def test_create_incident_gcp(
        self,
        dummy_gitlab_client: Gitlab,
        random_plugin: Dict[str, Any],
        random_asset: Dict[str, Any],
        random_vulnerability: Dict[str, Any],
        mocked_gitlab_utils_get_plugin_issue: MagicMock,
        mocked_gitlab_project_issue_manager_create: MagicMock,
        mocked_gitlab_project_link_issue_manager_create: MagicMock,
    ) -> None:
        random_asset["gcp_project_id"] = ["123"]
        random_asset["gcp_zone"] = ["us-east-1"]

        dummy_gitlab_utils = gitlab_utils(dummy_gitlab_client)
        dummy_gitlab_utils.create_or_update_incident(
            random_plugin, random_asset, random_vulnerability
        )

        assert (
            f"https://console.cloud.google.com/compute/instancesDetail/zones/us-east-1/instances/{random_asset['hostname'][0]}?project=123"
            in mocked_gitlab_project_issue_manager_create.call_args[0][0][
                "description"
            ]
        )

    def test_create_incident_ipv4(
        self,
        dummy_gitlab_client: Gitlab,
        random_plugin: Dict[str, Any],
        random_asset: Dict[str, Any],
        random_vulnerability: Dict[str, Any],
        mocked_gitlab_utils_get_plugin_issue: MagicMock,
        mocked_gitlab_project_issue_manager_create: MagicMock,
        mocked_gitlab_project_link_issue_manager_create: MagicMock,
    ) -> None:
        random_asset["hostname"].pop()
        random_asset["fqdn"].pop()

        dummy_gitlab_utils = gitlab_utils(dummy_gitlab_client)
        dummy_gitlab_utils.create_or_update_incident(
            random_plugin, random_asset, random_vulnerability
        )

        assert (
            random_asset["ipv4"][0]
            in mocked_gitlab_project_issue_manager_create.call_args[0][0][
                "title"
            ]
        )

    def test_create_incident_fqdn(
        self,
        dummy_gitlab_client: Gitlab,
        random_plugin: Dict[str, Any],
        random_asset: Dict[str, Any],
        random_vulnerability: Dict[str, Any],
        mocked_gitlab_utils_get_plugin_issue: MagicMock,
        mocked_gitlab_project_issue_manager_create: MagicMock,
        mocked_gitlab_project_link_issue_manager_create: MagicMock,
    ) -> None:
        random_asset["hostname"].pop()

        dummy_gitlab_utils = gitlab_utils(dummy_gitlab_client)
        dummy_gitlab_utils.create_or_update_incident(
            random_plugin, random_asset, random_vulnerability
        )

        assert (
            random_asset["fqdn"][0]
            in mocked_gitlab_project_issue_manager_create.call_args[0][0][
                "title"
            ]
        )


class TestGetAssetIncident:
    def test_get_asset_incident_existing_plugin(
        self,
        dummy_gitlab_client: Gitlab,
        dummy_gitlab_project_manager: ProjectManager,
        random_plugin: Dict[str, Any],
        random_asset: Dict[str, Any],
        mocked_gitlab_utils_get_plugin_issue: MagicMock,
        mocked_gitlab_project_issue_manager_get: MagicMock,
        mocked_gitlab_utils__get_open_vulnerability_links_for_plugin: MagicMock,
    ) -> None:
        dummy_gitlab_utils = gitlab_utils(dummy_gitlab_client)
        plugin_id = random_plugin["id"]
        asset_hostname = random_asset["hostname"][0]

        # Existing plugin
        dummy_plugin_issue = ProjectIssue(
            dummy_gitlab_project_manager, attrs={"title": "Dummy Pluin"}
        )
        mocked_gitlab_utils_get_plugin_issue.return_value = dummy_plugin_issue

        # Existing link to vulnerability
        dummy_plugin_issue_link = ProjectIssueLink(
            dummy_gitlab_project_manager,
            attrs={
                "state": "opened",
                "title": asset_hostname,
                "source_issue": 0,
                "target_issue": 0,
                "iid": 0,
            },
        )
        mocked_gitlab_utils__get_open_vulnerability_links_for_plugin.return_value = [
            dummy_plugin_issue_link
        ]

        # Must return the existing incident
        res = dummy_gitlab_utils.get_asset_incident(plugin_id, asset_hostname)
        # Must have gotten the list of links
        assert (
            mocked_gitlab_utils__get_open_vulnerability_links_for_plugin.assert_called_once
        )
        # Must have gotten the incident details
        mocked_gitlab_project_issue_manager_get.call_args[0][0] == 0
        mocked_gitlab_project_issue_manager_get.call_count == 1
        # Must have found and closed the opened issue
        assert res is not None

    def test_get_asset_incident_existing_plugin_no_opened(
        self,
        dummy_gitlab_client: Gitlab,
        dummy_gitlab_project_manager: ProjectManager,
        random_plugin: Dict[str, Any],
        random_asset: Dict[str, Any],
        mocked_gitlab_utils_get_plugin_issue: MagicMock,
        mocked_gitlab_utils__get_open_vulnerability_links_for_plugin: MagicMock,
        mocked_gitlab_project_issue_manager_get: MagicMock,
    ) -> None:
        dummy_gitlab_utils = gitlab_utils(dummy_gitlab_client)
        plugin_id = random_plugin["id"]
        asset_hostname = random_asset["hostname"][0]

        # Existing plugin
        dummy_plugin_issue = ProjectIssue(
            dummy_gitlab_project_manager, attrs={"title": "Dummy Pluin"}
        )
        mocked_gitlab_utils_get_plugin_issue.return_value = dummy_plugin_issue

        # Existing link to vulnerability, but "closed"
        dummy_plugin_issue_link = ProjectIssueLink(
            dummy_gitlab_project_manager,
            attrs={
                "state": "closed",
                "title": asset_hostname,
                "source_issue": 0,
                "target_issue": 0,
                "iid": 0,
            },
        )
        mocked_gitlab_utils__get_open_vulnerability_links_for_plugin.return_value = [
            dummy_plugin_issue_link
        ]

        # Must NOT return an existing incident
        res = dummy_gitlab_utils.get_asset_incident(plugin_id, asset_hostname)
        # Must have gotten the list of links
        assert (
            mocked_gitlab_utils__get_open_vulnerability_links_for_plugin.assert_called_once
        )
        # Must have no calls for details, because there are no "opened" incidents
        mocked_gitlab_project_issue_manager_get.assert_not_called
        # Must have found and closed the opened issue
        assert res is None


# TODO: Test create_incident. Should call our create plugin, the libraries
# issue.create and our link
# TODO: Test get_plugin_issue. Should call get issue on library.
# TODO: Test get_incident. Should call get plugin, get links and get issue.
# TODO: Test link. Should call the link function with the right asset_id
# and plugin_id.
