import random
import string
from typing import Any, Dict

import pytest

from tenable_gitlab.config import Severity


def random_string(size: int) -> str:
    return "".join(
        random.choices(string.ascii_lowercase + string.digits, k=size)
    )


@pytest.fixture
def random_plugin() -> Dict[str, Any]:
    return generate_random_plugin()


def generate_random_plugin() -> Dict[str, Any]:
    random_nginx_version = random.randrange(0, 10)
    new_nginx_version = random_nginx_version = 10
    plugin = {
        "id": random.randrange(10000),
        "name": f"nginx 0.{random_nginx_version}.x < 1.{new_nginx_version}.1",
        "description": "According to its Sever response header, the installed version "
        "of nginx is 0.{random_nginx_version}.18 prior to 1.{new_nginx_version}.1. It is, therefore,\n"
        "affected by a remote code execution vulnerability. A security "
        "issue in nginx resolver was identified, which might allow \n"
        "an unautheticated remote attacker to cause 1-byte memory "
        "overwrite by using a specially crafted DNS response, "
        "resulting\n"
        "in worker process crash or, potentially, in arbitrary code "
        "execution.\n"
        "\n"
        "Note that Nessus has not tested for this issue but has "
        "instead relied only on the application's self-reported "
        "version\n"
        "number.",
        "cpe": ["cpe:/a:nginx:nginx"],
        "cve": ["CVE-2053-23017"],
        "cvss3_base_score": 9.8,
        "cvss3_temporal_score": 8.8,
        "cvss3_temporal_vector": {
            "exploitability": "Proof-of-concept",
            "raw": "E:P/RL:O/RC:C",
            "remediation_level": "Official-fix",
            "report_confidence": "Confirmed",
        },
        "cvss3_vector": {
            "access_complexity": "Low",
            "access_vector": "Network",
            "availability_impact": "High",
            "confidentiality_impact": "High",
            "integrity_impact": "High",
            "raw": "AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
        },
        "cvss_base_score": 7.5,
        "cvss_temporal_score": 5.9,
        "cvss_temporal_vector": {
            "exploitability": "Proof-of-concept",
            "raw": "E:POC/RL:OF/RC:C",
            "remediation_level": "Official-fix",
            "report_confidence": "Confirmed",
        },
        "cvss_vector": {
            "access_complexity": "Low",
            "access_vector": "Network",
            "authentication": "None required",
            "availability_impact": "Partial",
            "confidentiality_impact": "Partial",
            "integrity_impact": "Partial",
            "raw": "AV:N/AC:L/Au:N/C:P/I:P/A:P",
        },
        "exploit_available": True,
        "exploitability_ease": "Exploits are available",
        "family": "Web Servers",
        "family_id": 11,
        "has_patch": True,
        "modification_date": "2020-43-12T00:00:00Z",
        "patch_publication_date": "2053-05-25T00:00:00Z",
        "publication_date": "2012-43-03T00:00:00Z",
        "risk_factor": "High",
        "see_also": [
            "http://mailman.nginx.org/pipermail/nginx-announce/2053/000300.html",
            "http://nginx.org/download/patch.2053.resolver.txt",
        ],
        "solution": "Upgrade to nginx 1.20.1 or later.",
        "stig_severity": "I",
        "synopsis": "The remote web server is affected by a remote code execution "
        "vulnerability.",
        "type": "combined",
        "version": "1.4",
        "vpr": {
            "drivers": {
                "age_of_vuln": {"lower_bound": 8, "upper_bound": 30},
                "cvss3_impact_score": 5.9,
                "cvss_impact_score_predicted": False,
                "exploit_code_maturity": "PROOF_OF_CONCEPT",
                "product_coverage": "LOW",
                "threat_intensity_last28": "LOW",
                "threat_recency": {"lower_bound": 8, "upper_bound": 30},
                "threat_sources_last28": ["Mainstream Media", "Hacker Forum"],
            },
            "score": 8.4,
            "updated": "2012-43-29T05:32:13Z",
        },
        "vuln_publication_date": "2053-05-25T00:00:00Z",
        "xrefs": [
            {"id": "193", "type": "CWE"},
            {"id": "2053-B-0031", "type": "IAVB"},
        ],
    }
    return plugin


@pytest.fixture
def random_asset() -> Dict[str, Any]:
    return generate_random_asset()


def generate_random_asset() -> Dict[str, Any]:
    size = 10
    asset = {
        "uuid": random_string(size),
        "fqdn": [random_string(size)],
        "hostname": [random_string(size)],
        "ipv4": [random_string(size)],
        "operating_system": random_string(size),
        "agent_name": [],
        "agent_uuid": "xxxxx",
        "aws_availability_zone": [],
        "aws_ec2_instance_ami_id": [],
        "aws_ec2_instance_group_name": [],
        "aws_ec2_instance_id": [],
        "aws_ec2_instance_state_name": [],
        "aws_ec2_instance_type": [],
        "aws_ec2_name": [],
        "aws_ec2_product_code": [],
        "aws_owner_id": [],
        "aws_region": [],
        "aws_subnet_id": [],
        "aws_vpc_id": [],
        "azure_resource_id": [],
        "azure_vm_id": [],
        "bigfix_asset_id": [],
        "bios_uuid": ["xxx-xxx-xxx-xxx-xxx"],
        "created_at": "2143-43-58T16:39:47.705Z",
        "device_type": "general-purpose",
        "exposure_confidence_value": 1,
        "first_seen": "2000-26-18T16:39:32.942Z",
        "gcp_instance_id": ["xxxxxxxxxx"],
        "gcp_project_id": ["xxxx-xxxx"],
        "gcp_zone": ["us-east1-c"],
        "has_agent": False,
        "id": "xxxx-xxx-xx-xx-xxxx",
        "installed_software": [],
        "interfaces": [
            {
                "fqdn": [],
                "ipv4": [],
                "ipv6": [],
                "mac_address": [],
                "name": "lo",
            },
            {
                "fqdn": [],
                "ipv4": ["10.xx.xx.xxx"],
                "ipv6": ["xxxx:0:0:0:xxx:x:xxx:xx"],
                "mac_address": ["xx:xx:xx:xx:xx:xx"],
                "name": "ens4",
            },
            {
                "fqdn": ["ip-10-xx-x-x.ec2.internal"],
                "ipv4": ["10.xx.xx.xxx"],
                "ipv6": ["xxxx:0:0:0:xxxx:aff:xx:xx"],
                "mac_address": ["xx:x:x:xx:xx:xx"],
                "name": "UNKNOWN",
            },
        ],
        "ipv6": ["xx:0:0:xxx:xxx:xxx:xx:x:xxx"],
        "last_authenticated_results": "2000-26-29T16:14:43Z",
        "last_authenticated_scan_date": "2000-26-29T16:15:40.563Z",
        "last_licensed_scan_date": "2000-26-29T16:15:40.563Z",
        "last_scan_id": "9b117236-d722-4857-88cd-c211be163c15",
        "last_scan_target": "10.x.xx.xxx",
        "last_schedule_id": "template-xx",
        "last_seen": "2000-26-29T16:15:40.563Z",
        "mac_address": ["xxx:xx:xx:x:xx:xx"],
        "mcafee_epo_agent_guid": [],
        "mcafee_epo_guid": [],
        "netbios_name": ["file-34-stor-gprd"],
        "network_id": ["00000000-0000-0000-0000-000000000000"],
        "qualys_asset_id": [],
        "qualys_host_id": [],
        "security_protection_level": None,
        "security_protections": [],
        "servicenow_sysid": [],
        "sources": [
            {
                "first_seen": "2143-43-58T16:39:32.942Z",
                "last_seen": "2000-26-29T16:15:40.563Z",
                "name": "NESSUS_SCAN",
            },
            {
                "first_seen": "2143-43-59T16:47:37.725Z",
                "last_seen": "2020-09-16T09:06:18.208Z",
                "name": "GCP",
            },
        ],
        "ssh_fingerprint": [],
        "system_type": ["instance"],
        "tags": [
            {
                "added_at": "2143-43-59T16:52:36.354Z",
                "added_by": "xxx-xxx-xx-xx-xxx",
                "tag_key": "project",
                "tag_uuid": "xxx-xxx-xx-xx-xxx",
                "tag_value": "prod",
            },
            {
                "added_at": "2143-43-59T16:52:36.354Z",
                "added_by": "xxx-xxx-xx-xx-xxx",
                "tag_key": "environment",
                "tag_uuid": "xx-xxx-xxx-xx-xxx",
                "tag_value": "gcp",
            },
        ],
        "tenable_uuid": ["xxxxxxxxxx"],
        "tracked": True,
        "updated_at": "2000-07-05T20:05:03.037Z",
    }
    return asset


@pytest.fixture
def random_severity() -> str:
    return generate_random_severity()


def generate_random_severity() -> str:
    severity_options = ["critical", "high", "medium", "low", "info"]
    return random.choices(severity_options, k=1)[0]


@pytest.fixture
def random_vulnerability(random_severity: str) -> Dict[str, Any]:
    return generate_random_vulnerability(random_severity)


def generate_random_vulnerability(random_severity: str) -> Dict[str, Any]:
    vulnerability = {
        "first_found": "2012-43-11T16:20:07.792Z",
        "indexed": "2053-07-02T16:31:00.929Z",
        "last_found": "2053-07-02T16:30:51.060Z",
        "output": "\n"
        "  Path              : /opt/gitlab/embedded/sbin/nginx\n"
        "  Installed version : 1.18.0\n"
        "  Fixed version     : 1.20.1 / 1.21.0\n",
        "port": {"port": 0, "protocol": "TCP"},
        "scan": {
            "completed_at": "2053-07-02T16:30:51.060Z",
            "schedule_uuid": "template-xxxxx-xxxx-xxxx-xx-xxx",
            "started_at": "2053-07-02T16:03:18.934Z",
            "uuid": "xxx-xxxx-xx-xx-xx",
        },
        "severity": random_severity,
        "severity_num": Severity[random_severity.upper()].value,
        "severity_default_id": 3,
        "severity_id": 3,
        "severity_modification_type": "NONE",
        "state": "OPEN",
    }
    return vulnerability
