import os
import random
import string
from typing import Generator

import pytest
from gitlab.base import RESTObject
from gitlab.client import Gitlab

from tenable_gitlab.config import Config
from tenable_gitlab.utils.gitlab import GitlabUtils


def random_string(size: int) -> str:
    return "".join(
        random.choices(string.ascii_lowercase + string.digits, k=size)
    )


@pytest.fixture(scope="module")
def gitlab_client() -> Gitlab:
    if "GITLAB_PRIVATE_TOKEN" not in os.environ:
        raise KeyError(
            "Please set the 'GITLAB_PRIVATE_TOKEN' environment variable"
        )

    token = os.environ["GITLAB_PRIVATE_TOKEN"]
    return Gitlab("https://gitlab.com", private_token=token)


@pytest.fixture
def gitlab_utils(
    gitlab_client: Gitlab, gitlab_project: RESTObject
) -> GitlabUtils:
    config = Config()
    glu = GitlabUtils(
        gitlab_client,
        gitlab_project.get_id(),
        config.incident_template_path,
        config.plugin_issue_template_path,
        config.default_incident_labels,
        config.default_plugin_labels,
    )
    return glu


@pytest.fixture(scope="module")
def gitlab_project(gitlab_client: Gitlab) -> Generator[RESTObject, None, None]:
    # generate a random project name
    name_size = 10
    name = random_string(name_size)

    # Setup: create a project in gitlab.com for testing
    project = gitlab_client.projects.create({"name": name})
    yield project

    # Teardown : delete the project
    gitlab_client.projects.delete(project.get_id())
