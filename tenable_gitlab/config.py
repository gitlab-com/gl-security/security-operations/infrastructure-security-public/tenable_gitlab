# annotations need to be postponed for typing to stop giving warnings
# https://stackoverflow.com/a/49872353
from __future__ import annotations

import os
from datetime import datetime, timedelta
from enum import IntEnum
from typing import List

CURRENT_FILE_PATH = os.path.abspath(os.path.dirname(__file__))


def timestamp_days_ago(num_days: int = 7) -> int:
    return int((datetime.now() - timedelta(days=num_days)).timestamp())


class Severity(IntEnum):
    CRITICAL = 1
    HIGH = 2
    MEDIUM = 3
    LOW = 4
    INFO = 5

    @staticmethod
    def list_of_severties_lower_than(severity: Severity) -> List[str]:
        """Creates a list with all the severities less critical than minimum_severity.

        Returns:
            List[str]: example: ["info", "low", "medium", "high", "critical"]
        """
        severities_list = [s.name.lower() for s in Severity if s >= severity]
        return severities_list

    @staticmethod
    def list_of_severties_bigger_than(
        severity: Severity,
    ) -> List[str]:
        """Creates a list with all the severities more critical than minimum_severity.

        Args:
            minimum_severity (Severity): the minimum severity

        Returns:
            List[str]: example: ["high", "critical"]
        """
        severities_list = [s.name.lower() for s in Severity if s <= severity]
        return severities_list


class Config:
    plugin_issue_template_path = os.path.join(
        CURRENT_FILE_PATH + "/templates/plugin_issue.jinja"
    )
    incident_template_path = os.path.join(
        CURRENT_FILE_PATH + "/templates/incident.jinja"
    )

    # Plugin labels.
    # Used to discover issues on GitLab for a given plugin
    default_plugin_labels = ["Tenable.io", "Tenable.io::Plugin"]

    # Incident labels
    # The defaults on this file try to map to the labels used by GitLab's program:
    # https://about.gitlab.com/handbook/engineering/security/vulnerability_management/#3-ingestion
    default_incident_labels = [
        "VM",
        "incident",
        "Tenable.io",
        "Tenable.io::Vulnerability",
    ]
    # labels applied when a new incident is opened
    open_incident_labels: List[str] = ["vulnerability::vulnerable"]
    # labels applied when a incident is fixed
    fixed_incident_labels: List[str] = ["vulnerability::remediated"]
    # labels applied when a incident is recasted
    recasted_incident_labels: List[str] = ["vulnerability::exception"]

    minimum_severity = Severity.HIGH
    observed_after = timestamp_days_ago()
    fixed_after = timestamp_days_ago()
    fixed_closing_reason = (
        "Closing this because the issue is marked as Fixed on Tenable.io"
    )

    auto_close_incidents_older_than = timestamp_days_ago(14)
