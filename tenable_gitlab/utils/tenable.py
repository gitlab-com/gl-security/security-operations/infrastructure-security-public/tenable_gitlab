from typing import Dict, Iterator, List, Tuple

from tenable.io import TenableIO  # type: ignore
from tenable.io.exports.iterator import ExportsIterator  # type: ignore


class TenableUtils:
    def __init__(self, tenable_client: TenableIO):
        self._tenable_client = tenable_client
        self._asset_details_cache: Dict[str, Dict] = {}

    def assets_iterator(
        self,
        observed_after: int,
        assets_chunk_size: int = 1000,
    ) -> Tuple[ExportsIterator, ExportsIterator, ExportsIterator]:
        """Exports the list of live, deleted and terminated assets from tenable

        Args:
            observed_after (int): Unix Timestamp to filter assets for (live only).
            assets_chunk_size (int, optional): Limit of assets retrieved. Defaults to 1000.

        Returns:
            Tuple[ExportsIterator, ExportsIterator, ExportsIterator]:
                Three iterables that can be used to access the details of all:
                    * Live assets
                    * Deleted assets
                    * Terminated assets
        """
        live = self._tenable_client.exports.assets(
            updated_at=observed_after,
            chunk_size=assets_chunk_size,
        )
        deleted = self._tenable_client.exports.assets(
            deleted_at=observed_after, chunk_size=assets_chunk_size
        )

        terminated = self._tenable_client.exports.assets(
            terminated_at=observed_after, chunk_size=assets_chunk_size
        )

        return live, deleted, terminated

    def open_vulnerabilities_iterator(
        self, observed_after: int, severities: List[str]
    ) -> ExportsIterator:
        """Exports the list of open vulnerabilities since `observed_after` timestamp

        Args:
            observed_after (int):  Unix Timestamp to filter vulnerabilities for.

        Returns:
            ExportsIterator: An interator that can be used to access the datails of the vulnerabilities.
        """
        vulns = self._tenable_client.exports.vulns(
            state=["open", "reopened"],
            last_found=observed_after,
            severity=severities,
            include_unlicensed=False,
            when_done=True,
            timeout=60,
            num_assets=5000,
        )
        return vulns

    def recasted_vulnerabilities_iterator(
        self, observed_after: int, severities: List[str]
    ) -> Iterator[Dict]:
        """Exports the list of recasted vulnerabilities since `observed_after` timestamp

        Args:
            observed_after (int):  Unix Timestamp to filter vulnerabilities for.

        Returns:
            List[Disct]: A list with the details of the vulnerabilities
        """
        vulns = self._tenable_client.exports.vulns(
            state=["open", "reopened"],
            last_found=observed_after,
            severity=severities,
            include_unlicensed=False,
            when_done=True,
            timeout=60,
            num_assets=5000,
        )
        for v in vulns:
            if "recast_rule_uuid" in v:
                yield v

    def fixed_vulnerabilities_iterator(
        self, fixed_after: int, severities: List[str]
    ) -> ExportsIterator:
        """Exports the list of open vulnerabilities since `fixed_after` timestamp

        Args:
            fixed_after (int):  Unix Timestamp to filter vulnerabilities for.

        Returns:
            ExportsIterator: An interator that can be used to access the datails of the vulnerabilities.
        """
        vulns = self._tenable_client.exports.vulns(
            state=["fixed"],
            last_fixed=fixed_after,
            severity=severities,
            include_unlicensed=False,
            when_done=True,
            timeout=60,
            num_assets=5000,
        )
        return vulns

    def get_asset_details(self, asset_uuid: str) -> Dict[str, str]:
        """Retrieve extra details from a given asset from Tenable.io

        Args:
            asset_uuid (str): The asset uuid

        Returns:
            Dict[str, str]: The details of the asset
        """
        if asset_uuid in self._asset_details_cache:
            return self._asset_details_cache[asset_uuid]

        details = self._tenable_client.assets.details(asset_uuid)
        self._asset_details_cache[asset_uuid] = details
        return details
