# annotations need to be postponed for typing to stop giving warnings
# https://stackoverflow.com/a/49872353
from __future__ import annotations

import logging
from typing import Any, Dict

from gitlab import Gitlab
from tenable.io import TenableIO

from tenable_gitlab.config import Config, Severity

from .utils.gitlab import GitlabUtils
from .utils.tenable import TenableUtils

logger = logging.getLogger(__name__)


class Tenable2Gitlab:
    def __init__(
        self,
        tenable_utils: TenableUtils,
        gitlab_utils: GitlabUtils,
        config: Config = Config(),
    ):
        """
        Perform the vuln ingestion and transformation.

        Args:
            tenable_utils (TenableUtils): Instance of TenableUtils
            gitlab_utils (GitlabUtils): instance of GitLabUtils
            config Config: Configurations
        """
        self._tenable_utils = tenable_utils
        self._gitlab_utils = gitlab_utils
        self._config = config

    @classmethod
    def create_from_clients(
        cls,
        tenable_client: TenableIO,
        gitlab_client: Gitlab,
        gitlab_project_id: int,
        config: Config = Config(),
    ) -> Tenable2Gitlab:
        """Constructor
        Returns an instance of Tenable2Gitlab given the GitLab client
        and TenableIO clients

        Args:
            tenable_client (TenableIO): A TenableIO client
            gitlab_client (Gitlab): A Gitlab client
            gitlab_project_id (int): The id of the project in gitlab where
                all issues and incidents will be created.
            config (Config, optional): [description]. Defaults to Config().

        Returns:
            Tenable2Gitlab: [description]
        """
        tenable_utils = TenableUtils(tenable_client)
        gitlab_utils = GitlabUtils(
            gitlab_client,
            gitlab_project_id,
            config.incident_template_path,
            config.plugin_issue_template_path,
            config.default_incident_labels,
            config.default_plugin_labels,
        )
        return cls(tenable_utils, gitlab_utils, config)

    def sync(self) -> None:
        """Perform the vuln ingestion and transformation."""
        self.sync_open_issues()
        self.sync_fixed_issues()
        self.sync_recasted_issues()
        self.auto_close_untracked_incidents()

    def sync_open_issues(self) -> None:
        logger.info(
            f"Starting syncronization for open vulnerabilities observed_since(timestamp): {self._config.observed_after} and minimum_severity: {self._config.minimum_severity.name}"
        )

        severities_list = Severity.list_of_severties_bigger_than(
            self._config.minimum_severity
        )

        open_vulnerabilities_iterator = (
            self._tenable_utils.open_vulnerabilities_iterator(
                self._config.observed_after, severities_list
            )
        )

        for vulnerability in open_vulnerabilities_iterator:
            plugin: Dict[str, Any] = vulnerability.pop("plugin")
            asset: Dict[str, Any] = vulnerability.pop("asset")

            severity = Severity[vulnerability["severity"].upper()]
            vulnerability["severity_num"] = severity.value

            # Get more details for the asset
            asset_details = self._tenable_utils.get_asset_details(
                asset["uuid"]
            )
            asset.update(asset_details)

            logger.info(
                f'Found vulnerability. Plugin: {plugin["name"]}. Asset: {asset["hostname"]}'
            )

            logger.debug(
                f'Extra asset details for asset {asset["uuid"]}: {asset_details}',
            )

            self._gitlab_utils.create_or_update_incident(
                plugin, asset, vulnerability, self._config.open_incident_labels
            )

    def sync_fixed_issues(self) -> None:
        logger.info(
            f"Starting syncronization for fixed vulnerabilities observed_since(timestamp): {self._config.observed_after} and minimum_severity: {self._config.minimum_severity.name}"
        )

        severities_list = Severity.list_of_severties_bigger_than(
            self._config.minimum_severity
        )

        fixed_vulnerabilities_iterator = (
            self._tenable_utils.fixed_vulnerabilities_iterator(
                self._config.observed_after, severities_list
            )
        )

        for vulnerability in fixed_vulnerabilities_iterator:
            plugin: Dict[str, Any] = vulnerability.pop("plugin")
            asset: Dict[str, Any] = vulnerability.pop("asset")

            severity = Severity[vulnerability["severity"].upper()]
            vulnerability["severity_num"] = severity.value

            # Get more details for the asset
            asset_details = self._tenable_utils.get_asset_details(
                asset["uuid"]
            )
            asset.update(asset_details)

            logger.info(
                f'Found FIXED vulnerability. Plugin: {plugin["name"]}. Asset: {asset["hostname"]}'
            )

            self._gitlab_utils.close_incident(
                plugin,
                asset,
                vulnerability,
                self._config.fixed_closing_reason,
                self._config.fixed_incident_labels,
            )

    def sync_recasted_issues(self) -> None:
        logger.info(
            f"Starting syncronization for recasted vulnerabilities observed_since(timestamp): {self._config.observed_after} and minimum_severity lower than: {self._config.minimum_severity.name}"
        )
        severities = Severity.list_of_severties_lower_than(
            self._config.minimum_severity
        )
        recasted_vulnerabilities = (
            self._tenable_utils.recasted_vulnerabilities_iterator(
                self._config.observed_after, severities
            )
        )

        for vulnerability in recasted_vulnerabilities:
            plugin: Dict[str, Any] = vulnerability.pop("plugin")
            asset: Dict[str, Any] = vulnerability.pop("asset")
            # Get more details for the asset
            asset_details = self._tenable_utils.get_asset_details(
                asset["uuid"]
            )
            asset.update(asset_details)

            severity = Severity[vulnerability["severity"].upper()]
            vulnerability["severity_num"] = severity.value

            recast_reason = vulnerability["recast_reason"]

            if severity > self._config.minimum_severity:
                logger.info(
                    f'Found RECASTED vulnerability with Severity less than {self._config.minimum_severity.name}. Plugin: {plugin["name"]}. Asset: {asset["hostname"]}. Last found: {vulnerability["last_found"]}'
                )
                self._gitlab_utils.close_incident(
                    plugin,
                    asset,
                    vulnerability,
                    recast_reason,
                    self._config.recasted_incident_labels,
                )

    def auto_close_untracked_incidents(self) -> None:
        logger.info(
            f"Starting auto-closing untracked incidents older than: {self._config.auto_close_incidents_older_than}"
        )
        self._gitlab_utils.auto_close_updated_before(
            self._config.auto_close_incidents_older_than,
            self._config.fixed_incident_labels,
        )
