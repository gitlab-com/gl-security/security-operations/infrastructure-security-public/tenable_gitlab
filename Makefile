setup:
	python -m venv .venv && \
		pip install -r test-requirements.txt

test: setup
	pytest test/unit/

it-test: setup
	pytest test/unit/
