from setuptools import find_packages, setup

with open(".version", "r") as fh:
    version = fh.read()

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt") as f:
    requirements = f.read().splitlines()


setup(
    name="tenable-gitlab",
    version=version,
    description="Tenable -> Gitlab integration",
    author="Gitlab Inc.",
    long_description=long_description,
    # author_email='xxx@gitlab.com',
    url="https://gitlab.com/",
    license="MIT",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Information Technology",
        "Topic :: System :: Networking",
        "Topic :: Other/Nonlisted Topic",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
    ],
    python_requires=">=3.7",
    keywords="tenable tenable_io gitlab",
    packages=find_packages(exclude=["test"]),
    package_data={"": ["templates/*.jinja"]},
    install_requires=requirements,
)
